provider "aws" {
  region     = "us-east-1"
  access_key = "AKIASMIKMN65B7QHMKXK"
  secret_key = "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"
}


# Create a VPC
resource "aws_vpc" "dev-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    "Name"    = "development vpc"
    "vpc_env" = "dev"
  }
}

variable "subnet_cidr_block" {
  description = "subnet cidr block"
}

# Create a Subnet referencing to an existing VPC
resource "aws_subnet" "dev-subnet-1" {
  vpc_id            = aws_vpc.dev-vpc.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = "us-east-1a"
  tags = {
    "Name" = "subnet 1"
  }
}

data "aws_vpc" "default_vpc" {
  default = true
}

resource "aws_subnet" "dev-subnet-2" {
  vpc_id            = data.aws_vpc.default_vpc.id
  cidr_block        = "172.31.48.0/20"
  availability_zone = "us-east-1a"
  tags = {
    "Name" = "subnet 2"
  }
}

output "dev-vpc-id" {
  value = aws_vpc.dev-vpc.id
}
output "dev-subnet-id" {
  value = aws_subnet.dev-subnet-1.id
}
